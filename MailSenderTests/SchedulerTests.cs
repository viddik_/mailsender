﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MailSender;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace MailSender.Tests
{
    [TestClass()]
    public class SchedulerTests
    {
        static Scheduler scheduler;
        static TimeSpan tsDefault;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            scheduler = new Scheduler();
            scheduler.SendDates = new Dictionary<DateTime, string>
            {
                { new DateTime(2018, 3, 12, 10, 0, 0), "text1" },
                { new DateTime(2018, 3, 12, 10, 5, 0), "text2" },
                { new DateTime(2018, 3, 12, 10, 10, 0), "text3" }

            };
            tsDefault = new TimeSpan();
        }

        [TestMethod()]
        public void StrToTimeSpan_empty_tsDefault()
        {
            string str = "";
            TimeSpan tsTest = scheduler.StrToTimeSpan(str);
            Assert.AreEqual(tsDefault, tsTest);
        }

        [TestMethod()]
        public void StrToTimeSpan_sdf_tsDefault()
        {
            string str = "sdf";
            TimeSpan tsTest = scheduler.StrToTimeSpan(str);
            Assert.AreEqual(tsDefault, tsTest);
        }

        [TestMethod()]
        public void StrToTimeSpan_correct_equal()
        {
            string str = "11:11";
            TimeSpan tsTest = scheduler.StrToTimeSpan(str);
            TimeSpan tsCorrect = new TimeSpan(11, 11, 0);
            Assert.AreEqual(tsCorrect, tsTest);
        }

        [TestMethod()]
        public void StrToTimeSpan_incorrectHour_tsDefault()
        {
            string str = "26:11";
            TimeSpan tsTest = scheduler.StrToTimeSpan(str);
            Assert.AreEqual(tsDefault, tsTest);
        }

        [TestMethod()]
        public void StrToTimeSpan_incorrectMinute_tsDefault()
        {
            string str = "11:62";
            TimeSpan tsTest = scheduler.StrToTimeSpan(str);
            Assert.AreEqual(tsDefault, tsTest);
        }

        [TestMethod()]
        public void Timer_Tick_dictionaryIsEmpty()
        {
            DateTime dt1 = new DateTime(2018, 3, 12, 10, 0, 0);
            DateTime dt2 = new DateTime(2018, 3, 12, 10, 5, 0);
            DateTime dt3 = new DateTime(2018, 3, 12, 10, 10, 0);

            DateTime firstKey = scheduler.SendDates.Keys.First<DateTime>();
            if (firstKey == dt1)
            {
                Debug.WriteLine("Body " + scheduler.SendDates[firstKey]);
                Debug.WriteLine("Subject " + $"Рассылка от {firstKey.ToShortDateString()} {firstKey.ToShortTimeString()}");
                scheduler.SendDates.Remove(firstKey);
            }

            firstKey = scheduler.SendDates.Keys.First<DateTime>();
            if (firstKey == dt2)
            {
                Debug.WriteLine("Body " + scheduler.SendDates[firstKey]);
                Debug.WriteLine("Subject " + $"Рассылка от {firstKey.ToShortDateString()} {firstKey.ToShortTimeString()}");
                scheduler.SendDates.Remove(firstKey);
            }

            firstKey = scheduler.SendDates.Keys.First<DateTime>();
            if (firstKey == dt3)
            {
                Debug.WriteLine("Body " + scheduler.SendDates[firstKey]);
                Debug.WriteLine("Subject " + $"Рассылка от {firstKey.ToShortDateString()} {firstKey.ToShortTimeString()}");
                scheduler.SendDates.Remove(firstKey);
            }

        }
    }
}