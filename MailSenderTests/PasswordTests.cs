﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MailSender;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailSender.Tests
{
    [TestClass()]
    public class PasswordTests
    {
        [TestMethod()]
        public void EncodeTest_empty_empty()
        {
            string str = Password.Encode(String.Empty);
            Assert.AreEqual(String.Empty, str, "Тест провален");
        }

        [TestMethod()]
        public void EncodeTest_abc_bcd()
        {
            string strIn = "abc";
            string str = Password.Encode(strIn);
            Assert.AreEqual("bcd", str);
        }

        [TestMethod()]
        public void DecodeTest_empty_empty()
        {
            string str = Password.Decode(String.Empty);
            Assert.AreEqual(String.Empty, str);
        }

        [TestMethod()]
        public void DecodeTest_bcd_abc()
        {
            string strIn = "bcd";
            string str = Password.Decode(strIn);
            Assert.AreEqual("abc", str);
        }
    }
}