﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using EmailSender;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.Reporting.WinForms;
using MailSender.MailSenderDataSetTableAdapters;

namespace MailSender
{
    /// <summary>
    /// Interaction logic for EmailSenderWindow.xaml
    /// </summary>
    public partial class EmailSenderWindow : Window
    {

        public ObservableCollection<Message> Messages = new ObservableCollection<Message>();

        public EmailSenderWindow()
        {
            string dataDir = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "db");
            if (!System.IO.Directory.Exists(dataDir))
                System.IO.Directory.CreateDirectory(dataDir);
            AppDomain.CurrentDomain.SetData("DataDirectory", dataDir);

            InitializeComponent();

            ecbSender.Keys = Variables.Senders.Keys;
            ecbSmtpServer.Keys = Variables.SmtpServers.Keys;

            lbMessages.ItemsSource = Messages;

            DBWork_ef db = new DBWork_ef();
            dgRecipients.ItemsSource = db.Recipients;

            btnAddMessage.Click += btnAddMessage_Click;

            btnSendAtOnce.Click += btnSendAtOnce_Click;
            btnSend.Click += btnSend_Click;

            tabSwitcher.ButtonNextClick += tabSwitcher_ButtonNextClick;
            tabSwitcher.ButtonPrevClick += tabSwitcher_ButtonPrevClick;

            reportViewer.Load += ReportViewerOnLoad;
        }

        private void miClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Добавление параметров письма для отправки
        /// </summary>
        private void btnAddMessage_Click(object sender, RoutedEventArgs e)
        {
            Message msg = new Message();
            msg.BodyText = "Some text";
            Messages.Add(msg);
        }

        /// <summary>
        /// Отправка письма
        /// </summary>
        private void btnSendAtOnce_Click(object sender, RoutedEventArgs e)
        {
            // Получаем и проверяем параметры отправки
            string login = ecbSender.Text;
            string password = Variables.Senders[login];
            string smtpServer = ecbSmtpServer.Text;
            int smtpPort = Variables.SmtpServers[smtpServer];
            TextRange selection = new TextRange(rtbBody.Document.ContentStart, rtbBody.Document.ContentEnd);
            string body = selection.Text;
            if (!CheckSendingParams(login, password, smtpServer, smtpPort))
                return;

            // Отправка писем
            EmailSendService emailSvc = new EmailSendService(login, password, smtpServer, smtpPort);
            emailSvc.Subject = "Test email from Dmitry Ivanov (C#-3, 2018-02)";
            emailSvc.Body = body;

            List<EmailSender.Recipient> recipiemts = new List<EmailSender.Recipient>();
            foreach (var item in dgRecipients.ItemsSource)
                recipiemts.Add(new EmailSender.Recipient(((Recipient)item).Name, ((Recipient)item).Email));

            emailSvc.SendMails(recipiemts);

            MsgWnd wnd = new MsgWnd("Задача завершена");
            wnd.Show();
        }

        /// <summary>
        /// Отправка письма по расписанию
        /// </summary>
        private void btnSend_Click(object sender, RoutedEventArgs e)
        {
            Scheduler scheduler = new Scheduler();

            // Создаем словарь параметров отправки для Scheduler
            // по данным коллекции Messages
            Dictionary<DateTime, string> sendDates = new Dictionary<DateTime, string>();
            foreach (var item in Messages)
            {
                // Проверка корректности введенного времени
                TimeSpan sendTime = scheduler.StrToTimeSpan(item.TimeString);
                if (sendTime == new TimeSpan())
                {
                    MsgWnd wnd = new MsgWnd("Некорректный формат даты", this);
                    wnd.Show();
                    return;
                }
                // Проверка корректности введенных даты и времени
                DateTime sendDate = (clndDates.SelectedDate ?? DateTime.Today).Add(sendTime);
                DateTime now = DateTime.Now;
                if (sendDate <= now)
                {
                    MsgWnd wnd = new MsgWnd("Время отправки писем не может быть раньше текущего времени", this);
                    wnd.Show();
                    return;
                }

                try
                {
                    sendDates.Add(sendDate, item.BodyText);
                }
                catch (ArgumentException)
                {
                    MsgWnd wnd = new MsgWnd($"Время {item.TimeString} уже присутствует в списке", this);
                    wnd.Show();
                    return;
                }
            }

            scheduler.SendDates = sendDates;

            // Получаем и проверяем параметры отправки
            string login = ecbSender.Text;
            string password = Variables.Senders[login];
            string smtpServer = ecbSmtpServer.Text;
            int smtpPort = Variables.SmtpServers[smtpServer];
            //TextRange selection = new TextRange(rtbBody.Document.ContentStart, rtbBody.Document.ContentEnd);
            
            if (!CheckSendingParams(login, password, smtpServer, smtpPort))
                return;

            // Отправка писем
            EmailSendService emailSvc = new EmailSendService(login, password, smtpServer, smtpPort);
            List<EmailSender.Recipient> recipiemts = new List<EmailSender.Recipient>();
            foreach (var item in dgRecipients.ItemsSource)
                recipiemts.Add(new EmailSender.Recipient(((Recipient)item).Name, ((Recipient)item).Email));

            scheduler.SendMails(emailSvc, recipiemts);
        }

        private bool CheckSendingParams(string login, string password, string smtpServer, int smtpPort)
        {
            // Проверка данных отправителя
            if (string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password))
            {
                MsgWnd wnd = new MsgWnd("Не выбран Отправитель", this);
                wnd.Show();
                return false;
            }

            // Проверка данных сервера
            if (string.IsNullOrEmpty(smtpServer) || smtpPort == 0)
            {
                MsgWnd wnd = new MsgWnd("Не выбран SMTP сервер", this);
                wnd.Show();
                return false;
            }

            // Проверка тела письма
            //if (string.IsNullOrEmpty(body.Replace("\r\n", "")))
            //{
            //    MsgWnd wnd = new MsgWnd("Письмо не заполнено", this);
            //    wnd.Show();
            //    tcTabs.SelectedItem = tabEditor;
            //    return false;
            //}

            return true;
        }

        /// <summary>
        /// Настройка ReportViewer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void ReportViewerOnLoad(object sender, EventArgs eventArgs)
        {
            ReportDataSource reportDataSource = new ReportDataSource();
            MailSenderDataSet dataset = new MailSenderDataSet();
            dataset.BeginInit();
            reportDataSource.Name = "DataSet";
            reportDataSource.Value = dataset.Recipient;
            reportViewer.LocalReport.DataSources.Add(reportDataSource);
            reportViewer.LocalReport.ReportPath = "../../RecipientsReport.rdlc";
            dataset.EndInit();
            RecipientTableAdapter tracksTableAdapter = new RecipientTableAdapter { ClearBeforeFill = true };
            tracksTableAdapter.Fill(dataset.Recipient);
            reportViewer.RefreshReport();
        }

        private void tabSwitcher_ButtonNextClick(object sender, RoutedEventArgs e)
        {
            tcTabs.SelectedIndex = Math.Min(tcTabs.SelectedIndex + 1, tcTabs.Items.Count - 1);
            RefreshTabSwitcherButtons();
        }

        private void tabSwitcher_ButtonPrevClick(object sender, RoutedEventArgs e)
        {
            tcTabs.SelectedIndex = Math.Max(tcTabs.SelectedIndex - 1, 0);
            RefreshTabSwitcherButtons();
        }

        private void RefreshTabSwitcherButtons()
        {
            tabSwitcher.IsHiddenNext = (tcTabs.SelectedIndex == tcTabs.Items.Count - 1);
            tabSwitcher.IsHiddenPrev = (tcTabs.SelectedIndex == 0);
        }

        private void tcTabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RefreshTabSwitcherButtons();
        }

        private void SchedulerControl_ButtonDeleteClick(object sender, RoutedEventArgs e)
        {
            if (lbMessages.SelectedItem != null)
                Messages.Remove((Message)lbMessages.SelectedItem);
        }
    }

    
}
