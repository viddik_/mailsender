﻿using System.ComponentModel;

namespace MailSender
{
    public class Message: INotifyPropertyChanged
    {
        private string timeString;
        public string TimeString
        {
            get => timeString;
            set
            {
                string s = (value ?? "").Trim();
                if (s != timeString)
                {
                    timeString = s;
                    NotifyPropertyChanged(nameof(TimeString));
                }
            }
        }

        private string bodyText;
        public string BodyText
        {
            get => bodyText;
            set
            {
                bodyText = value;
                NotifyPropertyChanged(nameof(BodyText));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged​(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
