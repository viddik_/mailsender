﻿using EmailSender;
using System.Data.Entity;

namespace MailSender
{
    public class SqlLocalDbContext : DbContext
    {
        public SqlLocalDbContext() : base("DBConnection")
        {
            Database.SetInitializer<SqlLocalDbContext>(new SqlLocalDbContextInitializer());
        }

        public DbSet<Recipient> Recipients { get; set; }
    }
}
