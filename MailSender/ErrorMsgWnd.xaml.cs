﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfTest
{
    /// <summary>
    /// Interaction logic for ErrorMsgWnd.xaml
    /// </summary>
    public partial class ErrorMsgWnd : Window
    {
        public ErrorMsgWnd()
        {
            InitializeComponent();
        }

        public ErrorMsgWnd(string message) : this()
        {
            txtblckMessage.Text = message;
        }

        public ErrorMsgWnd(string message, Exception ex) : this(message)
        {
            txtblckException.Text = ex.ToString();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
