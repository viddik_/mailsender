﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailSender
{
    public static class Password
    {
        public static string Encode(string str)
        {
            string cipher = "";
            foreach (char a in str)
            {
                char ch = a;
                ch++;
                cipher += ch;
            }
            return cipher;
        }

        public static string Decode(string cipher)
        {
            string str = "";
            foreach (char a in cipher)
            {
                char ch = a;
                ch--;
                str += ch;
            }
            return str;
        }
    }
}
