﻿using System.Data.Entity;
using System.Collections.Generic;
using EmailSender;

namespace MailSender
{
    public class SqlLocalDbContextInitializer : CreateDatabaseIfNotExists<SqlLocalDbContext>
    {
        protected override void Seed(SqlLocalDbContext context)
        {
            context.Recipients.AddRange(new List<Recipient>
            {
                new Recipient { Name = "spam1", Email = "spam1@ksergey.ru" },
                new Recipient { Name = "cdscds", Email = "cdscds@ksergey.ru" },
                new Recipient { Name = "qwerty", Email = "qwerty@ksergey.ru" },
                new Recipient { Name = "Мой адрес", Email = "megg1q6tjk5y@mail.ru" },
            });
            context.SaveChanges();
        }
    }
}
