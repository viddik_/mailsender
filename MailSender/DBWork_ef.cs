﻿using EmailSender;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailSender
{
    public class DBWork_ef
    {
        private SqlLocalDbContext dbContext = new SqlLocalDbContext();
        public List<Recipient> Recipients => dbContext.Recipients.ToList();
    }
}
