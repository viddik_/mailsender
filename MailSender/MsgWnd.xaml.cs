﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MailSender
{
    /// <summary>
    /// Interaction logic for MsgWnd.xaml
    /// </summary>
    public partial class MsgWnd : Window
    {
        public MsgWnd()
        {
            InitializeComponent();
        }

        public MsgWnd(string message) : this()
        {
            txtblckMessage.Text = message;
        }

        public MsgWnd(string message, Window owner) : this(message)
        {
            Owner = owner;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
