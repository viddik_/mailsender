﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailSender
{
    public static class Variables
    {
        private static Dictionary<string, string> senders = new Dictionary<string, string>()
        {
            { "test@ksergey.ru", Password.Decode("rxfsuz3129") }
        };

        public static Dictionary<string, string> Senders
        {
            get => senders;
        }

        private static Dictionary<string, int> smtpServers = new Dictionary<string, int>()
        {
            { "smtp.yandex.ru", 25 }
        };

        public static Dictionary<string, int> SmtpServers
        {
            get => smtpServers;
        }
    }
}
