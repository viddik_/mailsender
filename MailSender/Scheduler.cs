﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Threading;

namespace MailSender
{
    /// <summary>
    /// Планировщик, реализует отправку писем по таймеру.
    /// </summary>
    public class Scheduler
    {
        DispatcherTimer timer = new DispatcherTimer();
        EmailSender.EmailSendService emailSvc;
        DateTime dateSend;
        IEnumerable<EmailSender.Recipient> recipients;

        Dictionary<DateTime, string> sendDates = new Dictionary<DateTime, string>();
        public Dictionary<DateTime, string> SendDates
        {
            get => sendDates;
            set
            {
                sendDates = value;
                sendDates = sendDates.OrderBy(pair => pair.Key).ToDictionary(pair => pair.Key, pair => pair.Value);
            }
        }

        public Scheduler()
        {
        }

        public Scheduler(EmailSender.EmailSendService emailService, IEnumerable<EmailSender.Recipient> recipients)
        {
            emailSvc = emailService;
            this.recipients = recipients;
        }

        /// <summary>
        /// Преобразует строку, представляющую время, в TimeSpan
        /// </summary>
        /// <param name="strTime"></param>
        /// <returns></returns>
        public TimeSpan StrToTimeSpan(string strTime)
        {
            TimeSpan ret = new TimeSpan();
            try
            {
                ret = TimeSpan.Parse(strTime);
            }
            catch
            {
            }
            return ret;
        }

        public void SendMails(EmailSender.EmailSendService emailService, IEnumerable<EmailSender.Recipient> recipients)
        {

            if (emailService == null || recipients == null)
                return;

            emailSvc = emailService;
            this.recipients = recipients;

            timer.Tick += Timer_Tick;
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (sendDates.Count == 0)
            {
                timer.Stop();
                MsgWnd wnd = new MsgWnd("Все задачи завершены");
                wnd.Show();
            }
            else 
            {
                DateTime firstDate = sendDates.Keys.First<DateTime>();
                if (firstDate <= DateTime.Now)
                {
                    try
                    {
                        emailSvc.Subject = $"Рассылка от {firstDate.ToShortDateString()} {firstDate.ToShortTimeString()}";
                        emailSvc.Body = sendDates[firstDate];
                        emailSvc.SendMails(recipients);
                    }
                    finally
                    {
                        sendDates.Remove(firstDate);
                    }
                }
            }
        }
    }
}
