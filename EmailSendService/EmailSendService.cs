﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;

namespace EmailSender
{
    /// <summary>
    /// Реализация отправки писем по smtp
    /// </summary>
    public class EmailSendService
    {
        public string Sender { get; set; } = "";

        public string Password { get; set; } = "";

        public string SmtpServerAddr { get; set; } = "";

        public int SmtpServerPort { get; set; } = 25;

        public List<string> Recipients { get; set; } = new List<string>();

        public string Subject { get; set; } = "";

        public string Body { get; set; } = "";

        public EmailSendService()
        {
        }

        public EmailSendService(string sender, string password)
        {
            Sender = sender;
            Password = password;

        }

        public EmailSendService(string sender, string password, string smtpServerAddr, int smtpServerPort) : this(sender, password)
        {
            SmtpServerAddr = smtpServerAddr;
            SmtpServerPort = (0 < smtpServerPort && smtpServerPort <= 65535) ? smtpServerPort : 25;
        }

        /// <summary>
        /// Отправка письма одному адресату
        /// </summary>
        /// <param name="recepient"></param>
        /// <param name="name"></param>
        public SendingResult SendMail(string recepient, string name = "")
        {
            SendingResult res = new SendingResult(recepient);
            using (MailMessage mm = new MailMessage(Sender, recepient))
            {
                mm.Subject = Subject;
                mm.Body = Body + "\n\n Отправлено " + DateTime.Now.ToString();
                mm.IsBodyHtml = false;
                using (SmtpClient client = new SmtpClient(SmtpServerAddr, SmtpServerPort))
                {
                    client.EnableSsl = true;
                    client.Credentials = new NetworkCredential(Sender, Password);
                    try
                    {
                        client.Send(mm);
                        res.Message = "ОК";
                    }
                    catch (Exception ex)
                    {
                        res.Message = "Ошибка";
                        res.Ex = ex;
                    }
                    res.Time = DateTime.Now;
                }
            }
            return res;
        }

        /// <summary>
        /// Отправка письма коллекции адресатов
        /// </summary>
        /// <param name="recepients"></param>
        public List<SendingResult> SendMails(IEnumerable<Recipient> recepients)
        {
            List<SendingResult> results = new List<SendingResult>();
            foreach (var recepient in recepients)
            {
                SendingResult res = SendMail(recepient.Email, recepient.Name);
                results.Add(res);
            }
            return results;
        }
    }
}
