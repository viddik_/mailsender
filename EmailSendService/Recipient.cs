﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EmailSender
{
    [Table("Recipient")]
    public class Recipient
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Email { get; set; }

        public Recipient() : this("", "")
        {
        }

        public Recipient(string name, string email)
        {
            Name = name;
            Email = email;
        }
    }
}
