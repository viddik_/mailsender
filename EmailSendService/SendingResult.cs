﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmailSender
{
    public class SendingResult
    {
        public DateTime Time { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
        public Exception Ex { get; set; }

        public SendingResult(string email = "")
        {
            Email = email;
        }
    }
}
