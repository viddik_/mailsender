﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExtComboBox
{
    /// <summary>
    /// Interaction logic for ExtComboBoxControl.xaml
    /// </summary>
    public partial class ExtComboBoxControl : UserControl
    {
        /// <summary>
        /// Label Content
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// ComboBox ToolTip
        /// </summary>
        public string ToolTipText { get; set; }

        /// <summary>
        /// ComboBox ItemsSource
        /// </summary>
        public IEnumerable Keys
        {
            get => comboBox.ItemsSource;
            set
            {
                comboBox.ItemsSource = value;
                comboBox.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// ComboBox Text
        /// </summary>
        public string Text
        {
            get => comboBox.Text;
        }

        /// <summary>
        /// ComboBox SelectedItem
        /// </summary>
        public object SelectedItem { get => comboBox.SelectedItem; }

        // Buttons events
        public event RoutedEventHandler ButtonAddClick;
        public event RoutedEventHandler ButtonEditClick;
        public event RoutedEventHandler ButtonDeleteClick;

        // ComboBox events
        public event SelectionChangedEventHandler ComboBox_SelectionChanged;

        public ExtComboBoxControl()
        {
            InitializeComponent();
            DataContext = this;

            btnAdd.Click += btnAdd_Click;
            btnEdit.Click += btnEdit_Click;
            btnDelete.Click += btnDelete_Click;
        }

        public ExtComboBoxControl(IEnumerable keys) : this()
        {
            Keys = keys;
        }

        #region Проброс событий наружу

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            ButtonAddClick?.Invoke(sender, e);
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            ButtonEditClick?.Invoke(sender, e);
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            ButtonDeleteClick?.Invoke(sender, e);
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox_SelectionChanged?.Invoke(sender, e);
        }

        #endregion


    }
}
