﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPL
{
    /// <summary>
    /// Пример вычисления произведения квадратных матриц
    /// в параллельных потоках с помощью Tasks
    /// </summary>
    class Program
    {
        static int[,] a;
        static int[,] b;
        static int[,] c;
        static object locker = new object();

        static Random rand = new Random();

        static void Main(string[] args)
        {
            int n = 100;
            a = GetRandomMatrix(n, -5, 15);
            b = GetRandomMatrix(n, -10, 10);
            c = new int[n, n];

            // Создаем отдельную задачу на вычисление каждого элемента
            // результирующей матрицы
            Task[] tasks = new Task[n*n];
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int i = 0; i <= a.GetUpperBound(0); i++)
            {
                for (int j = 0; j <= b.GetUpperBound(1); j++)
                {
                    tasks[i * n + j] = Task.Factory.StartNew(CalcMultiElement, new Point(i, j));
                }
            }

            // Ожидаем выполнения всех задач
            try
            {
                Task.WaitAll(tasks);
            }
            catch (AggregateException ae)
            {
                Console.WriteLine("One or more exceptions occurred: ");
                foreach (var ex in ae.Flatten().InnerExceptions)
                    Console.WriteLine(ex.Message);
            }
            
            watch.Stop();

            Console.WriteLine($"Done in {watch.Elapsed.Seconds} s {watch.Elapsed.Milliseconds} ms.\nPress any key to close.");
            Console.ReadKey(true);
        }

        /// <summary>
        /// Генерация квадратной матрицы со случайными целыми элементами
        /// </summary>
        /// <param name="size"></param>
        /// <param name="fromValue"></param>
        /// <param name="toValue"></param>
        /// <returns></returns>
        public static int[,] GetRandomMatrix(int size, int fromValue, int toValue)
        {
            if (size < 2)
                throw new ArgumentOutOfRangeException("size", "Size is less than 2");

            int[,] result = new int[size, size];
            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    result[i, j] = rand.Next(fromValue, toValue + 1);

            return result;
        }

        /// <summary>
        /// Вычисляет элемент c[row, col] как векторное произведение 
        /// строки a[row,] на столбец b[, col]
        /// </summary>
        /// <param name="cell"></param>
        static void CalcMultiElement(object cell)
        {
            int row = ((Point)cell).X;
            int col = ((Point)cell).Y;
            int vectorMult = 0;
            for (int j = 0; j <= a.GetUpperBound(1); j++)
                for (int i = 0; i <= b.GetUpperBound(0); i++)
                    vectorMult += a[row, j] * b[i, col];

            lock (locker)
            {
                c[row, col] = vectorMult;
            }

            Console.WriteLine($"Task {Task.CurrentId}, row {row}, col {col}, result {vectorMult}");
        }
    }
}
