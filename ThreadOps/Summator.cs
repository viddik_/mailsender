﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadOps
{
    /// <summary>
    /// Демонстрирует применение многопоточности для вычислений
    /// 1. суммы всех целых чисел от 1 до N 
    /// 2. Факториала N
    /// </summary>
    public class Summator
    {
        private long N;
        private int sign;

        private TaskTypes taskType;

        private int chunkSize;
        private long taskCounter;

        private long result;
        public long Result => result;

        private object lockCounter = new object();
        private object lockResult = new object();

        public event Action<string> OnMessage;

        public Summator(TaskTypes taskType, long n)
        {
            this.taskType = taskType;
            if (taskType == TaskTypes.Sum)
            {
                result = 0;
                sign = Math.Sign(n);
                chunkSize = 1000000;
            }
            else if (taskType == TaskTypes.Factorial)
            {
                result = 1;
                sign = 1;
                chunkSize = 5;
            }

            N = Math.Abs(n);
        }

        /// <summary>
        /// Выполняет многопоточное вычисление
        /// </summary>
        public void Calculate()
        {
            // Определяем количество подзадач
            long chunks = N / chunkSize;
            long taskCounter_ = chunks + ((N % chunkSize) > 0 ? 1 : 0);

            // Запускаем задачи в пуле потоков
            ThreadPool.GetAvailableThreads(out int avWorkThreads, out int avIOThreads);
            ThreadPool.GetMaxThreads(out int maxWorkThreads, out int maxIOThreads);
            OnMessage?.Invoke($"Используем {taskCounter_} потоков, в пуле доступно {avWorkThreads} из {maxWorkThreads}");
            for (int i = 1; i <= taskCounter_; i++)
            {
                long start = 1 + (i - 1) * chunkSize;
                long finish;
                if (i <= chunks)
                    finish = i * chunkSize;
                else
                    finish = N;

                CalcTask task = new CalcTask(taskType, start, finish, this);
                if (ThreadPool.QueueUserWorkItem(task.Do))
                    IncreaseCounter();
            }

            // Ожидаем завершения всех задач
            lock (lockCounter)
            {
                while (taskCounter > 0)
                {
                    OnMessage?.Invoke($"Работает {taskCounter} потоков...");
                    Monitor.Wait(lockCounter);
                }
            }

            result *= sign;
        }

        public void AddResult(long r)
        {
            lock (lockResult)
            {
                result += r;
            }
        }

        public void MultResult(long r)
        {
            lock (lockResult)
            {
                result *= r;
            }
        }

        public void IncreaseCounter()
        {
            lock (lockCounter)
            {
                taskCounter++;
            }
        }

        public void DecreaseCounter()
        {
            lock (lockCounter)
            {
                taskCounter--;
                Monitor.Pulse(lockCounter);
            }
        }
    }
}
