﻿using System.Threading;

namespace ThreadOps
{
    class CalcTask
    {
        private long from;
        private long to;
        private long result;
        private TaskTypes taskType;
        private Summator summator;

        public CalcTask(TaskTypes taskType, long start, long finish, Summator summator)
        {
            this.summator = summator ?? throw new SummatorException("Parameter 'summator' cannot be null");

            if (taskType == TaskTypes.Sum)
                result = 0;
            else if (taskType == TaskTypes.Factorial)
                result = 1;

            this.taskType = taskType;
            from = start;
            to = finish;
        }

        public void Do(object state)
        {
            try
            {
                if (taskType == TaskTypes.Sum)
                    Sum();
                else if (taskType == TaskTypes.Factorial)
                    Mult();
            }
            finally
            {
                summator.DecreaseCounter();
            }
        }

        void Sum()
        {
            for (long i = from; i <= to; i++)
            {
                result += i;
            }
            summator.AddResult(result);
        }

        void Mult()
        {
            for (long i = from; i <= to; i++)
            {
                result *= i;
            }
            summator.MultResult(result);
        }
    }
}
