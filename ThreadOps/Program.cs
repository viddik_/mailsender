﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreadOps
{
    class Program
    {
        static object lockCounter = new object();
        static object lockResult = new object();

        static void Main(string[] args)
        {
            Console.WriteLine("Что будем делать?");
            Console.WriteLine("\t1 - вычисление суммы от 1 до N (разбиение - 1 000 000 чисел на поток)");
            Console.WriteLine("\t2 - вычисление факториала N (разбиение - 5 чисел на поток)");
            Console.WriteLine("\tq - выход");

            bool inputOk = false;
            string choice = "";
            while (!inputOk)
            {
                Console.Write("Ваш выбор (1 | 2 | q):");
                choice = Console.ReadLine().ToLower().Trim();
                if (choice == "1" || choice == "2")
                    inputOk = true;
                else if (choice == "q")
                    return;
            }

            long N = 0;
            inputOk = false;
            if (choice == "1" || choice == "2")
            {
                while (!inputOk)
                {
                    Console.Write("Задайте N (целое число больше нуля) или нажмите q для выхода:");
                    string s = Console.ReadLine().ToLower().Trim();
                    if (s == "q")
                        return;
                    inputOk = long.TryParse(s, out N);
                }
            }

            Summator summator;
            if (choice == "1")
            {
                summator = new Summator(TaskTypes.Sum, N);
            }
            else
            {
                summator = new Summator(TaskTypes.Factorial, N);
            }
            summator.OnMessage += PrintLn;
            summator.Calculate();

            Console.WriteLine($"Результат: {summator.Result}");
            Console.WriteLine("Нажмите любую клавишу для выхода");
            Console.ReadKey(true);

        }

        static void PrintLn(string s)
        {
            Console.WriteLine(s);
        }
    }
}
