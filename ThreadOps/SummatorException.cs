﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreadOps
{
    public class SummatorException: Exception
    {
        public SummatorException(): base()
        {
        }

        public SummatorException(string message): base(message)
        {
        }
    }
}
