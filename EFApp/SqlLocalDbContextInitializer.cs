﻿using System.Collections.Generic;
using System.Data.Entity;

namespace EFApp
{
    public class SqlLocalDbContextInitializer : CreateDatabaseIfNotExists<SqlLocalDbContext>
    {
        protected override void Seed(SqlLocalDbContext context)
        {
            context.Tracks.AddRange(new List<Track>
            {
                new Track { ArtistName = "Артист 1", TrackName = "Трек 1" },
            });
            context.SaveChanges();
        }
    }
}
