﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFApp
{
    [Table("Tracks")]
    public class Track
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string ArtistName { get; set; }

        [Required]
        public string TrackName { get; set; }
    }
}
