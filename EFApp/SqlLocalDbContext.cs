﻿using System.Data.Entity;

namespace EFApp
{
    public class SqlLocalDbContext : DbContext
    {
        public SqlLocalDbContext() : base("DBConnection")
        {
            Database.SetInitializer(new SqlLocalDbContextInitializer());
            //Database.Initialize(true);
        }

        public DbSet<Track> Tracks { get; set; }
    }
}
