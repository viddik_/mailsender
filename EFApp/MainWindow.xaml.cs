﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Entity;
using Microsoft.Reporting.WinForms;
using EFApp._Tracks_db_mdfDataSetTableAdapters;

namespace EFApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SqlLocalDbContext dbContext;

        public List<Track> TrackList;
        
        public MainWindow()
        {
            InitializeComponent();

            string dataDir = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "db");
            if (!System.IO.Directory.Exists(dataDir))
                System.IO.Directory.CreateDirectory(dataDir);
            AppDomain.CurrentDomain.SetData("DataDirectory", dataDir);
            
            dbContext = new SqlLocalDbContext();

            reportViewer.Load += ReportViewerOnLoad;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ReloadTrackList();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(tbArtistName.Text) && !String.IsNullOrWhiteSpace(tbTrackName.Text))
            {
                AddNewTrack();
                ReloadTrackList();
            }

        }

        private void ReloadTrackList()
        {
            dbContext.Tracks.Load();
            dgTracks.DataContext = dbContext.Tracks.Local;
        }

        private void AddNewTrack()
        {
            dbContext.Tracks.Add(
                new Track() {
                    ArtistName = tbArtistName.Text.Trim(),
                    TrackName = tbTrackName.Text.Trim()
                });
            dbContext.SaveChanges();
        }

        private void ReportViewerOnLoad(object sender, EventArgs eventArgs)
        {
            ReportDataSource reportDataSource = new ReportDataSource();
            _Tracks_db_mdfDataSet dataset = new _Tracks_db_mdfDataSet();
            dataset.BeginInit();
            reportDataSource.Name = "DataSet1";
            reportDataSource.Value = dataset.Tracks;
            reportViewer.LocalReport.DataSources.Add(reportDataSource);
            reportViewer.LocalReport.ReportPath = "../../TracksReport.rdlc";
            dataset.EndInit();
            TracksTableAdapter tracksTableAdapter = new TracksTableAdapter { ClearBeforeFill = true };
            tracksTableAdapter.Fill(dataset.Tracks);
            reportViewer.RefreshReport();
        }

    }
}
