﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TabSwitcher
{
    /// <summary>
    /// Interaction logic for TabSwitcherControl.xaml
    /// </summary>
    public partial class TabSwitcherControl : UserControl
    {
        private bool isHiddenPrev;
        private bool isHiddenNext;

        /// <summary>
        /// Флаг скрытия кнопки Назад
        /// </summary>
        public bool IsHiddenPrev
        {
            get => isHiddenPrev;
            set
            {
                isHiddenPrev = value;
                SetButtons();
            }
        }

        /// <summary>
        /// Флаг скрытия кнопки Вперед
        /// </summary>
        public bool IsHiddenNext
        {
            get => isHiddenNext;
            set
            {
                isHiddenNext = value;
                SetButtons();
            }
        }

        public event RoutedEventHandler ButtonNextClick;
        public event RoutedEventHandler ButtonPrevClick;

        public TabSwitcherControl()
        {
            InitializeComponent();

            btnNext.Click += btnNext_Click;
            btnPrev.Click += btnPrev_Click;
        }

        private void ShowAllButtons()
        {
            btnNext.Visibility = Visibility.Visible;
            btnNext.Width = 105;
            btnPrev.Visibility = Visibility.Visible;
            btnPrev.Width = 105;
        }

        private void HideAllButtons()
        {
            btnNext.Visibility = Visibility.Hidden;
            btnPrev.Visibility = Visibility.Hidden;
            
        }

        private void HideNextButton()
        {
            btnNext.Visibility = Visibility.Hidden;
            btnNext.Width = 0;
            btnPrev.Visibility = Visibility.Visible;
            btnPrev.Width = 210;
            btnPrev.HorizontalAlignment = HorizontalAlignment.Stretch;
        }

        private void HidePrevButton()
        {
            btnNext.Visibility = Visibility.Visible;
            btnNext.Width = 210;
            btnNext.HorizontalAlignment = HorizontalAlignment.Stretch;
            btnPrev.Visibility = Visibility.Hidden;
            btnPrev.Width = 0;
        }

        /// <summary>
        /// Отрисовка кнопок
        /// </summary>
        private void SetButtons()
        {
            if (!IsHiddenNext && !IsHiddenPrev)
                ShowAllButtons();
            else if (!IsHiddenNext && IsHiddenPrev)
                HidePrevButton();
            else if (IsHiddenNext && !IsHiddenPrev)
                HideNextButton();
            if (IsHiddenNext && IsHiddenPrev)
                HideAllButtons();
        }

        private void btnPrev_Click(object sender, RoutedEventArgs e)
        {
            ButtonPrevClick?.Invoke(sender, e);
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            ButtonNextClick?.Invoke(sender, e);
        }
    }
}
