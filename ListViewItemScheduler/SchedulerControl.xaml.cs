﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ListViewItemScheduler
{
    /// <summary>
    /// Interaction logic for SchedulerControl.xaml
    /// </summary>
    public partial class SchedulerControl : UserControl
    {
        public string TimeString
        {
            get => (string)GetValue(TimeStringProperty);
            set 
            {
                SetValue(TimeStringProperty, value);
            }
        }

        public static readonly DependencyProperty TimeStringProperty = DependencyProperty.Register(
            nameof(TimeString), typeof(string), typeof(SchedulerControl), new UIPropertyMetadata(""), TimeStringPropertyChanged);

        private static bool TimeStringPropertyChanged(object value)
        {
            return true;
        }

        public event RoutedEventHandler ButtonEditClick;
        public event RoutedEventHandler ButtonDeleteClick;

        public SchedulerControl()
        {
            InitializeComponent();

            btnEdit.Click += btnEdit_Click;
            btnDelete.Click += btnDelete_Click;
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            ButtonEditClick?.Invoke(sender, e);
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            ButtonDeleteClick?.Invoke(sender, e);
        }
    }
}
